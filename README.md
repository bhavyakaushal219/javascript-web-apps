## ExpressJs with MySQL, Postgres and Sequelize


Run the app:

```
docker-compose up
```
Then navigate to http://localhost:3000/


Stop all the running containers

```
docker compose down
```

To stop and remove all containers, networks, images used by services in docker-compose.yml file:

```
docker compose down --rmi all
```

DB Entries (If needed for changing request parameter values):

```
(id, username, nickname, age) -> (1, "Phil Dunphy", "philly", 50);
(id, username, nickname, age) -> (2, "Claire Dunphy", "Clairy", 45);
(id, username, nickname, age) -> (3, "Luke Dunphy", "Lukey", 5);
(id, username, nickname, age) -> (4, "Alex Dunphy", "Alexx", 15);
(id, username, nickname, age) -> (5, "Hailey Dunphy", "Hail", 23);
```
Rule link : https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/rules/lgpl/javascript/database/rule-node_sqli_injection.yml?ref_type=heads
