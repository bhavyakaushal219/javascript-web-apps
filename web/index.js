const express = require("express");
const app = express();
const port = 3000;

require("./mysql_injection")(app);
require("./pg_injection")(app);
require("./sequelize_injection")(app);

app.listen(port, () => {
  console.log(`Express app listening at http://localhost:${port}`);
});

// app.get("/", (req, res) => {
//   res.send("Hello World!");
// });

app.get("/", (req, res) => {
  const links = [
    {
      url: "/mysql/one?username=Phil+Dunphy&nickname=philly",
      text: "MySQL test 1",
    },
    { url: "/mysql/two?id=4", text: "MySQL test 2" },
    {
      url: "/mysql/three?id=5",
      text: "MySQL test 3",
    },
    { url: "/pg/one?id=4", text: "Postgres test 1" },
    {
      url: "/pg/two?nickname=Lukey",
      text: "Postgres test 2",
    },
    { url: "/seq/one?id=2", text: "Sequelize test 1" },
    {
      url: "/seq/two?username=Phil+Dunphy&nickname=philly",
      text: "Sequelize test 2",
    },
    {
      url: "/seq/three?username=Phil+Dunphy&nickname=philly",
      text: "Sequelize test 3",
    },
    {
      url: "/seq/four?username=Phil+Dunphy&nickname=philly",
      text: "Sequelize test 4",
    },
    { url: "/seq/five", text: "Sequelize test 5" },
    {
      url: "/seq/six?username=Phil+Dunphy&nickname=philly",
      text: "Sequelize test 6",
    },
    {
      url: "/seq/seven?username=Phil+Dunphy&nickname=philly",
      text: "Sequelize test 7",
    },
    {
      url: "/seq/eight",
      text: "Sequelize test 8",
    },
    // Add more links as needed
  ];

  // Start with an HTML structure
  let htmlResponse = `
        <html>
            <head>
                <title>API Endpoints</title>
                <style>
                    body { font-family: Arial, sans-serif; }
                    ul { list-style-type: none; }
                    li { margin: 10px 0; }
                    a { text-decoration: none; color: blue; }
                    a:hover { text-decoration: underline; }
                </style>
            </head>
            <body>
                <h1>Available API Endpoints</h1>
                <p> 
                Feel free to change values of query parameters.
                Check the Readme file to see the db entries. 
                P.S. The request queries might look similar, but the db queries behind are structured in different formats to test for varied patterns.
                </p>
                <ul>`;

  // Add each link as a list item
  links.forEach((link) => {
    htmlResponse += `<li><a href="${link.url}">${link.text}</a></li>`;
  });

  // Close the HTML tags
  htmlResponse += `
                </ul>
            </body>
        </html>`;

  // Send the response
  res.send(htmlResponse);
});
