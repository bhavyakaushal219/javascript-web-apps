CREATE TABLE myuser (
    id SERIAL PRIMARY KEY,
    username VARCHAR(60),
    nickname VARCHAR(80),
    age INT
);

INSERT INTO myuser (username, nickname, age) VALUES ('Phil Dunphy', 'philly', 50);
INSERT INTO myuser (username, nickname, age) VALUES ('Claire Dunphy', 'Clairy', 45);
INSERT INTO myuser (username, nickname, age) VALUES ('Luke Dunphy', 'Lukey', 5);
INSERT INTO myuser (username, nickname, age) VALUES ('Alex Dunphy', 'Alexx', 15);
INSERT INTO myuser (username, nickname, age) VALUES ('Hailey Dunphy', 'Hail', 23);
